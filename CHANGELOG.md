# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.3] - 2022-06-01 (ghiorso)
### Changed
- Updated code remove debug printout in setlogfO2/S2
- Updated setup.py accordingly 

## [1.0.2] - 2020-07-30 (ghiorso)
### Changed
- Updated code to remove writable string deprecations
- Updated CMAKE compilation flags
- Updated setup.py accordingly 

## [1.0.1] - 2020-07-13 (ghiorso)
### Changed
- Finalized CI scripts 

## [1.0.0] - 2020-07-07 (ghiorso)
### Added
- Initial release